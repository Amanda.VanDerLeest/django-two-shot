from django.shortcuts import redirect

# from django.views.generic.base import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView

from receipts.models import ExpenseCategory, Account, Receipt


# Create your views here.
# class ReceiptListView(LoginRequiredMixin, TemplateView):
#     model = Receipt
#     template_name = "receipts/list.html"


class ExpenseCategoryCreateView(CreateView, LoginRequiredMixin):
    model = ExpenseCategory
    fields = ["name", "owner"]
    template_name = "receipts/new.html"

    def form_valid(self, form):
        expense = form.save(commit=False)
        expense.owner = self.request.user
        expense.save()
        return redirect("home")


class AccountCreateView(CreateView, LoginRequiredMixin):
    model = Account
    fields = ["name", "number", "owner"]
    template_name = "receipts/new.html"

    def form_valid(self, form):
        account = form.save(commit=False)
        account.owner = self.request.user
        account.save()
        return redirect("home")


class ReceiptCreateView(CreateView, LoginRequiredMixin):
    model = Receipt
    fields = ["vendor", "total", "tax", "date", "category", "account"]
    template_name = "receipts/new.html"

    def form_valid(self, form):
        receipt = form.save(commit=False)
        receipt.purchaser = self.request.user
        receipt.save()
        return redirect("home")


class ExpenseCategoryListView(ListView, LoginRequiredMixin):
    model = ExpenseCategory
    template_name = "receipts/expense_list.html"
    context_object_name = "expense_list"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)


class AccountListView(ListView, LoginRequiredMixin):
    model = Account
    template_name = "receipts/account_list.html"
    context_object_name = "accounts_list"

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)


class ReceiptListView(ListView, LoginRequiredMixin):
    model = Receipt
    template_name = "receipts/list.html"
    context_object_name = "receipts_list"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


# does receiptlistview with (templateveiw) get deleted?
