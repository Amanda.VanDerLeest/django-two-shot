from receipts.views import (
    AccountListView,
    ExpenseCategoryListView,
    ReceiptListView,
    ReceiptCreateView,
    ExpenseCategoryCreateView,
    AccountCreateView,
)
from django.urls import path

urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("create/", ReceiptCreateView.as_view(), name="receipt_create"),
    path(
        "categories/",
        ExpenseCategoryListView.as_view(),
        name="expense_category_list",
    ),
    path(
        "categories/create/",
        ExpenseCategoryCreateView.as_view(),
        name="expense_create",
    ),
    path("accounts/", AccountListView.as_view(), name="account_list"),
    path(
        "accounts/create/", AccountCreateView.as_view(), name="account_create"
    ),
]
